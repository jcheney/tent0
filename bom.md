# BOM - Version 1a

## Fabric

- ~8 yd - 1.1 oz silicone-impregnated polyester, XL wide - [RBTR Store](https://ripstopbytheroll.com/collections/waterproof-polyester-fabric/products/1-1-oz-silpoly-xl?variant=35045467789)
- Tube - GearAid silicone sealant - [RBTR Store](https://ripstopbytheroll.com/products/sil-net-silicone-seam-sealer)

## Hardware

- 2 - 18" carbon pole with insert, .296" dia [Quest Outfitters Store](https://questoutfitters.com/Tent_Poles_CF_3.9.htm#3.9_Carbon_Fiber_Poles_&_Parts)
- 2 - 18" carbon pole w/o insert, .296" dia [Quest Outfitters Store](https://questoutfitters.com/Tent_Poles_CF_3.9.htm#3.9_Carbon_Fiber_Poles_&_Parts)
- 4 - Rubber end caps [Quest Outfitters Store](https://questoutfitters.com/Tent_Poles_CF_3.9.htm#3.9_Carbon_Fiber_Poles_&_Parts)
- Metal grommets - Local fabric shop
- Webbing - Local fabric shop

## Others - TBD
