![](drawings/0a_perspective.jpg)

# What is it?

A lightweight tent with fly, tub, mosquito net that can be used independently or combined.

# Plans

Primarily done using Grid Mode in [DaveCAD](https://github.com/EEVblog/DaveCAD). Basic trigonometry also involved. See [CAD files](drawings/drawings.md)

# BOM

See [bom.md](bom.md)

# Procedure

1. Create plans. In classic JC fashion, don't fully flush them out until the implementation has already been started. Go by the seat of your pants!
2. Create a scale model using paper or fabric to make sure everything works.
3. Get a general idea of how pieces will be laid out on stock to minimize waste. 
4. Cut out pieces. I used a small tape measure and a sharpie to create small dotted lines and mark vertices - [photo](process_photos/20220117_102625.jpg). Then cut out using a knife.
    - I cut out the first section using the incorrect value for a side length (It was the length of that section projected onto the ground instead of the "diagonal" length.) Re-ordered some more silpoly - Not cheap!
5. TBD
