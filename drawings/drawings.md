# Brainstorming

![](potential_designs_0.jpg)

![](potential_designs_1.jpg)

![](potential_designs_2.jpg)

# Initial Design (0a)

![](0a_top_ortho.jpg)

![](0a_perspective.jpg)

![](0a_layout.jpg)

![](0a_dimensions.jpg)

(Model pic here)

# Revision (1a)

![](0a_changes.jpg)

![](1a_top_ortho.jpg)

![](1a_notes.jpg)

# Model

![](1a_model_pattern.jpg)
